package websocket.zengcode.com.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import websocket.zengcode.com.model.Payload;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class SchedulerTask {

    private SimpMessagingTemplate template;

    @Autowired
    public SchedulerTask(SimpMessagingTemplate template) {
        this.template = template;
    }

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 1000)
    public void sendMessageToClient() {

        this.template.convertAndSend("/topic/greetings", new Payload("Time is : " + dateFormat.format(new Date()) ));
    }


}
