package websocket.zengcode.com.model;

/**
 * Created by pea.chiwa on 2/14/17.
 */
public class Payload {

    private String content;

    public Payload() {
    }

    public Payload(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
